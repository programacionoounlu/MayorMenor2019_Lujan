package ar.edu.unlu.mayormenor.controlador;

import ar.edu.unlu.mayormenor.modelo.CambiosEnElJuego;
import ar.edu.unlu.mayormenor.modelo.IJugador;
import ar.edu.unlu.mayormenor.modelo.Juego;
import ar.edu.unlu.mayormenor.modelo.Observador;

public class ControladorMyM implements Observador{
	private Juego miJuego = Juego.getInstance();
	private Visualizable miVista;
	public ControladorMyM(Visualizable miVista) {
		this.miVista = miVista;
		miJuego.agregarObservador(this);
		miVista.mostrarMenuSeteando();
		miVista.setControlador(this);
		miVista.comenzar();
	}
	
	@Override
	public void actualizar(CambiosEnElJuego cambioEstado) {
		switch (cambioEstado){
			case HAY_NUEVO_JUGADOR:
				miVista.mostrarListaJugadores(miJuego.getJugadores());
				break;
			case JUEGO_INICIADO:
				miVista.mostrarMenuJugando();

			case ES_CORRECTO:
				miVista.mostrarVasGanando();
				mostrarJugadorYCarta();
				
				break;
			case CAMBIO_TURNO:
				miVista.mostrarAhCobarde();
				mostrarJugadorYCarta();
				break;
			case PERDISTE:
				miVista.mostrarPerdiste();
				mostrarJugadorYCarta();
				break;
			case FIN_JUEGO:
				miVista.mostrarMenuFinJuego();
				break;
			
		}
	}

	private void mostrarJugadorYCarta() {
		miVista.mostrarCartaEnJuego(miJuego.UltimaCarta());
		miVista.mostrarJugadorEnTurno(miJuego.getJugadorActual());
	}
	
	public void agergarJugador(String nombre) {
		miJuego.agregarJugador(nombre);
	}

	public void iniciarJuego() {
		miJuego.iniciar();
	}
	
	public void mayor() {
		// TODO Auto-generated method stub
		miJuego.mayor();
	}

	public void menor() {
		// TODO Auto-generated method stub
		miJuego.menor();
		
	}

	public void paso() {
		// TODO Auto-generated method stub
		miJuego.paso();
		
	}
}
