package ar.edu.unlu.mayormenor.controlador;

import ar.edu.unlu.mayormenor.modelo.Carta;
import ar.edu.unlu.mayormenor.modelo.IJugador;

public interface Visualizable {
	void mostrarMenuSeteando();
	void mostrarMenuJugando();
	void mostrarMenuFinJuego();
	void mostrarListaJugadores(IJugador[] jugadores);
	void mostrarJugadorEnTurno(IJugador jugador);
	void mostrarCartaEnJuego(Carta carta);
	void mostrarVasGanando();
	void mostrarPerdiste();
	void mostrarAhCobarde();
	void comenzar();
	void setControlador(ControladorMyM ctl);
}
