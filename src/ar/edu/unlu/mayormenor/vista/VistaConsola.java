package ar.edu.unlu.mayormenor.vista;

import java.util.Scanner;

import ar.edu.unlu.mayormenor.controlador.ControladorMyM;
import ar.edu.unlu.mayormenor.controlador.Visualizable;
import ar.edu.unlu.mayormenor.modelo.Carta;
import ar.edu.unlu.mayormenor.modelo.IJugador;

public class VistaConsola implements Visualizable {
	
	private static final int SETEANDO = 0;
	private static final int JUGANDO = 1;
	private static final int FIN_JUEGO = 2;
	private static final int TERMINAR = 3;
	private int estado;
	private ControladorMyM miControlador;

	
	
	public VistaConsola() {
		super();
		estado = SETEANDO;
	}
	
	@Override
	public void setControlador(ControladorMyM miControlador) {
		this.miControlador = miControlador;
	}
	
	@Override
	public void comenzar() {
		do {
			mostarMenu();
			
		} while (estado != TERMINAR);
	}
	
	private void mostarMenu() {
		switch (estado) {
		case SETEANDO:
			mostarMenuSet();
			break;
		case JUGANDO:
			mostarMenuJug();
			break;
		case FIN_JUEGO:
			break;
		}
		
	}

	private void mostarMenuSet() {
		Scanner sc = new Scanner(System.in);
		String opcion;
		char opt;
		do {
			System.out.println("============================================");
			System.out.println("= MAYOR Y MENOR - EL JUEGO QUE TE APASIONA =");
			System.out.println("============================================");
			System.out.println("= 1 - Agregar Jugador                      =");
			System.out.println("= 2 - Comenzar Juego                       =");
			System.out.println("=------------------------------------------=");
			System.out.println("= F - Terminar el Juego                    =");
			System.out.println("============================================");
			System.out.println("=> Su opcion es :  ");
			opcion = sc.nextLine();
		} while (!valido(opcion,"1","2","F"));
		switch (opcion) {
		case "1":
			miControlador.agergarJugador(dameElNombre());
			break;
		case "2":
			miControlador.iniciarJuego();
			break;
		case "F":
			estado = TERMINAR;
			break;
			
		}
	}

	private void mostarMenuJug() {
		Scanner sc = new Scanner(System.in);
		String opcion;
		char opt;
		do {
			System.out.println("============================================");
			System.out.println("= MAYOR Y MENOR - EL JUEGO QUE TE APASIONA =");
			System.out.println("============================================");
			System.out.println("= 1 - Mayor                                =");
			System.out.println("= 2 - Menor                                =");
			System.out.println("= 3 - Paso                                 =");
			System.out.println("============================================");
			System.out.println("=> Su opcion es :  ");
			opcion = sc.nextLine();
		} while (!valido(opcion,"1","2","3"));
		switch (opcion) {
		case "1":
			miControlador.mayor();;
			break;
		case "2":
			miControlador.menor();
			break;
		case "3":
			miControlador.paso();
			break;
			
		}
	}

	
	private String dameElNombre() {
		Scanner sc = new Scanner(System.in);
		System.out.println("=> Tu nombre es :  ");
		String nombre = sc.nextLine();
		return nombre;
	}

	private boolean valido(String opcion, String ... valoresPosibles) {
		boolean respuesta = false;
		for(String s : valoresPosibles) {
			if(s.equals(opcion))
				respuesta = true;
		}
		return respuesta;
	}

	@Override
	public void mostrarMenuSeteando() {
		this.estado = SETEANDO;

	}

	@Override
	public void mostrarMenuJugando() {
		this.estado = JUGANDO;

	}

	@Override
	public void mostrarMenuFinJuego() {
		this.estado = FIN_JUEGO;

	}

	@Override
	public void mostrarListaJugadores(IJugador[] jugadores) {
		for(IJugador j : jugadores) {
			System.out.println(" - " + j.getNombre() + " - puntos :  " + j.getPuntos());
		}

	}

	@Override
	public void mostrarJugadorEnTurno(IJugador j) {
		System.out.println("Es el turno de .... ");
		System.out.println(" - " + j.getNombre() + " - puntos :  " + j.getPuntos());
	}

	@Override
	public void mostrarCartaEnJuego(Carta carta) {
		System.out.println("Tu carta es .... ");
		System.out.println(" - " + carta.getValorRepresentacion() + " de :  " + carta.getPalo());

	}

	@Override
	public void mostrarVasGanando() {
		System.out.println("Acertaste!!!! ");

	}

	@Override
	public void mostrarPerdiste() {
		System.out.println("No no no noooooo ");

	}

	@Override
	public void mostrarAhCobarde() {
		System.out.println("Ok ");


	}

}
