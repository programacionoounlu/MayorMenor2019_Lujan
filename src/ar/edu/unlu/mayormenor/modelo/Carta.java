package ar.edu.unlu.mayormenor.modelo;

public class Carta {
	private String palo;
	private String valorAMostrar;
	private int valorReal;
	public Carta(String palo, String valorAMostrar, int valorReal) {
		this.palo = palo;
		this.valorAMostrar = valorAMostrar;
		this.valorReal = valorReal;
	}

	public String getPalo() {
		return palo;
	}

	public String getValorRepresentacion() {
		return valorAMostrar;
	}

	public int getValor() {
		return valorReal;
	}

}
