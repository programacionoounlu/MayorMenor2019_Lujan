package ar.edu.unlu.mayormenor.modelo;

public enum CambiosEnElJuego {
	HAY_NUEVO_JUGADOR,
	ES_CORRECTO,
	CAMBIO_TURNO,
	PERDISTE,
	FIN_JUEGO,
	JUEGO_INICIADO
}
