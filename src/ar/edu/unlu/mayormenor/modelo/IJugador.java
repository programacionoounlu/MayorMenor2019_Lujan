package ar.edu.unlu.mayormenor.modelo;

public interface IJugador {

	String getNombre();

	int getPuntos();

}