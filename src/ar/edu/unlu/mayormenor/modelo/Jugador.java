package ar.edu.unlu.mayormenor.modelo;

public class Jugador implements IJugador {

	private String nombre;
	private int puntos = 0;
	public Jugador(String nombre) {
		this.nombre = nombre;
	}

	/* (non-Javadoc)
	 * @see ar.edu.unlu.mayormenor.modelo.IJugador#getNombre()
	 */
	@Override
	public String getNombre() {
		
		return nombre;
	}

	/* (non-Javadoc)
	 * @see ar.edu.unlu.mayormenor.modelo.IJugador#getPuntos()
	 */
	@Override
	public int getPuntos() {
		return puntos;
	}

	public void sumar(int puntos) {
		if(puntos > 0)
			this.puntos += puntos;
		
	}

	public void restar(int puntos) {
		if(puntos > 0)
			this.puntos -= puntos;
		
	}

	public void resetear() {
		puntos = 0;
		
	}

}
