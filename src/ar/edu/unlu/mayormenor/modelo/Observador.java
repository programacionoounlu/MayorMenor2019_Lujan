package ar.edu.unlu.mayormenor.modelo;

public interface Observador {

	void actualizar(CambiosEnElJuego cambioEstado);

}
