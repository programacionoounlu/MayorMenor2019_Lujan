package ar.edu.unlu.mayormenor.modelo;

import java.util.ArrayList;
import java.util.Random;

public class Mazo {
	
	private Carta[] misCartas;
	private String[] palos = {"Diamante","Trebol","Pica","Corazon"};
	private String[] valoresR = {"As","2","3","4","5","6","7","8","9","10","J","Q","K"};
	private int cartaActual = 0;
	
	public Mazo() {
		misCartas = new Carta[(palos.length * valoresR.length) + 2];
		int i = 0;
		int valor = 0;
		for(String p : palos) {
			valor = 1;
			for(String v : valoresR) {
				misCartas[i++] = new Carta(p,v,valor++);
			}
		}
		misCartas[i++] = new Carta("Jocker",";)",0);
		misCartas[i] = new Carta("Jocker",";)",0);
		//this.mostrarCartas();
	}
	
	public Carta getCarta() {
		if(cartaActual < misCartas.length)
			return misCartas[cartaActual];
		else
			return null;
	}
	
	public void siguiente() {
		cartaActual ++;
	}
	
	
	public void mezclar() {
		Random r = new Random();
		for(int j = 0;j < misCartas.length; j ++) {
			int np = r.nextInt(misCartas.length);
			Carta c = misCartas[np];
			misCartas[np] = misCartas[j];
			misCartas[j] = c;
		}
		cartaActual = 0;
	}
	
	public void mostrarCartas() {
		for(Carta c : misCartas)
			System.out.println(c.getPalo() + "  " + c.getValor() + " " + c.getValorRepresentacion());
	}

}
