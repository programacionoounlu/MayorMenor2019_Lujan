package ar.edu.unlu.mayormenor.modelo;

import java.util.ArrayList;

public class Juego {
	private Mazo miMazo = new Mazo();
	private Carta ultimaCarta;
	private final static int SETEANDO = 0;
	private final static int JUGANDO = 1;
	private final static int FIN_JUEGO = 2;
	private int puntosAcumulados = 0;
	private ArrayList<Jugador> misJugadores = new ArrayList<>();
	private int miEstado = SETEANDO;
	private ArrayList<Observador> misObservadores = new ArrayList<>();
	private int jugadorActual;
	private int puntosParaGanar = 100;
	private ArrayList<CambiosEnElJuego> misCambioEstado = new ArrayList<>();
	// Implementacion de Singleton //
	private static Juego instancia = new Juego();

	public static Juego getInstance() {
		return instancia;
	}

	private Juego() {

	}

	//
	public void agregarJugador(String nombre) {
		if (miEstado == SETEANDO) {
			misJugadores.add(new Jugador(nombre));
			notificarObservadores(CambiosEnElJuego.HAY_NUEVO_JUGADOR);
		}
	}

	// Implementacion del Observer //
	private void notificarObservadores(CambiosEnElJuego cambioEstado) {
		misCambioEstado.add(cambioEstado);
	}
	
	private void notificarAhora() {
		for (Observador o : misObservadores)
			for (CambiosEnElJuego c : misCambioEstado)
				o.actualizar(c);
		misCambioEstado.clear();

	}
	public void agregarObservador(Observador observador) {
		misObservadores.add(observador);
	}

	public IJugador[] getJugadores() {
		IJugador[] respuesta = new IJugador[misJugadores.size()];
		int jugador = 0;
		for (Jugador j : misJugadores)
			respuesta[jugador++] = j;
		return respuesta;
	}

	public void iniciar() {
		if (misJugadores.size() > 1) {
			miMazo.mezclar();
			ultimaCarta = miMazo.getCarta();
			miMazo.siguiente();
			jugadorActual = 0;
			miEstado = JUGANDO;
			notificarObservadores(CambiosEnElJuego.JUEGO_INICIADO);
			notificarAhora();
		}
	}

	public Carta UltimaCarta() {
		// TODO Auto-generated method stub
		return ultimaCarta;
	}

	public void mayor() {
		Carta nueva = darCarta();

		if (ultimaCarta.getValor() < nueva.getValor()) {
			puntosAcumulados += 10;
			notificarObservadores(CambiosEnElJuego.ES_CORRECTO);
		} else if (ultimaCarta.getValor() == nueva.getValor()) {
			notificarObservadores(CambiosEnElJuego.ES_CORRECTO);

		} else {
			misJugadores.get(jugadorActual).restar(puntosAcumulados);
			puntosAcumulados = 0;
			notificarObservadores(CambiosEnElJuego.PERDISTE);
			cambiarJugador();
		}
		ultimaCarta = nueva;
		notificarAhora();
	}

	public void menor() {
		Carta nueva = darCarta();

		if (ultimaCarta.getValor() > nueva.getValor()) {
			puntosAcumulados += 10;
			notificarObservadores(CambiosEnElJuego.ES_CORRECTO);
		} else if (ultimaCarta.getValor() == nueva.getValor()) {
			notificarObservadores(CambiosEnElJuego.ES_CORRECTO);

		} else {
			misJugadores.get(jugadorActual).restar(puntosAcumulados);
			puntosAcumulados = 0;
			notificarObservadores(CambiosEnElJuego.PERDISTE);
			cambiarJugador();
		}
		ultimaCarta = nueva;
		notificarAhora();
	}

	public void paso() {
		misJugadores.get(jugadorActual).sumar(puntosAcumulados);
		puntosAcumulados = 0;
		notificarObservadores(CambiosEnElJuego.CAMBIO_TURNO);
		cambiarJugador();
		notificarAhora();
	}	

	private void cambiarJugador() {
		jugadorActual++;
		if (jugadorActual == misJugadores.size()) {
			jugadorActual = 0;
			int maximo = misJugadores.get(0).getPuntos();
			int posicion = 0;
			for(int i = 1; i < misJugadores.size(); i++) {
				Jugador j = misJugadores.get(i);
				if (j.getPuntos() > maximo) {
					maximo = j.getPuntos();
					posicion = i;
				}
			}
			if(maximo >= puntosParaGanar) {
				notificarObservadores(CambiosEnElJuego.FIN_JUEGO);
				this.miEstado = FIN_JUEGO;
				jugadorActual = posicion;
			}
			
		}
		notificarAhora();
	}
	
	public void reiniciar() {
		miEstado = SETEANDO;
		jugadorActual = 0;
		puntosAcumulados = 0;
		//misJugadores.clear();
		for(Jugador j : misJugadores) {
			j.resetear();
		}
	}
	
	private Carta darCarta() {
		Carta nueva = miMazo.getCarta();
		if (nueva == null) {
			miMazo.mezclar();
			nueva = miMazo.getCarta();
		}
		miMazo.siguiente();

		if (nueva.getValor() == 0) {
			return this.darCarta();
		} else
			return nueva;

	}

	public IJugador getJugadorActual() {
		return misJugadores.get(jugadorActual);
	}
}
