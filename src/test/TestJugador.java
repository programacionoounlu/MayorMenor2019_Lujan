package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.unlu.mayormenor.modelo.Jugador;
import junit.framework.Assert;

public class TestJugador {
	Jugador j;
	@Before
	public void crerJugador() {
		 j=  new Jugador("Jugador 1");
	}
	
	@Test
	public void test_Jugador_nombre() {
		Assert.assertEquals("Jugador 1", j.getNombre());
	}

	@Test
	public void test_Jugador_puntos_0() {
		Assert.assertEquals(0, j.getPuntos());
		
	}
	
	@Test
	public void test_Jugador_puntos_1() {
		j.sumar(10);
		Assert.assertEquals(10, j.getPuntos());	
	}
	
	@Test
	public void test_Jugador_puntos_2() {
		j.sumar(10);
		j.sumar(10);
		Assert.assertEquals(20, j.getPuntos());	
	}
	
	@Test
	public void test_Jugador_puntos_3() {
		j.sumar(10);
		j.restar(5);
		Assert.assertEquals(5, j.getPuntos());	
	}	
	@Test
	public void test_Jugador_puntos_4() {
		j.sumar(-10);
		j.restar(5);
		Assert.assertEquals(-5, j.getPuntos());	
	}
	
	@Test
	public void test_Jugador_puntos_5() {
		j.sumar(10);
		j.restar(-5);
		Assert.assertEquals(10, j.getPuntos());	
	}
}
