package test;

import static org.junit.Assert.*;

import org.junit.Test;

import ar.edu.unlu.mayormenor.modelo.Carta;
import ar.edu.unlu.mayormenor.modelo.Mazo;
import junit.framework.Assert;

public class TestMazo {

	@Test
	public void testMaso_1() {
		Mazo m = new Mazo();
		Carta c = m.getCarta();
		Assert.assertEquals("Diamante", c.getPalo());
		Assert.assertEquals("As", c.getValorRepresentacion());
		Assert.assertEquals(1, c.getValor());
		m.mezclar();
	}

}
