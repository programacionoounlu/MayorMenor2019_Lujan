package test;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;

import ar.edu.unlu.mayormenor.modelo.Carta;

public class TestCarta {

	@Test
	public void testCarta_1() {
		Carta c = new Carta("Diamante","As",1);
		Assert.assertEquals("Diamante", c.getPalo());
		Assert.assertEquals("As", c.getValorRepresentacion());
		Assert.assertEquals(1, c.getValor());	
	}

}
