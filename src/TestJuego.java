import ar.edu.unlu.mayormenor.controlador.ControladorMyM;
import ar.edu.unlu.mayormenor.controlador.Visualizable;
import ar.edu.unlu.mayormenor.vista.VistaConsola;

public class TestJuego {

	public static void main(String[] args) {
		Visualizable vista = new VistaConsola(); 
		ControladorMyM c = new ControladorMyM(vista);
	}

}
